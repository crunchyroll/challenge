import java.util.Comparator;

/* Author: Craig Scheiderer
 * Date: 4/17/2017
 * Description: A class that can hold the device data of each JSON object and .csv data set.
 */

public class Device {
	
	String uuid;
    double latitude;
    double longitude;
    long timestamp;
	
    //4 parameter constructor for each device
	public Device(String uuidIn, Double latitudeIn, Double longitudeIn, 
			long timestampIn){
		this.uuid = uuidIn;
		this.latitude = latitudeIn;
		this.longitude = longitudeIn;
		this.timestamp = timestampIn;
	}
	
	//Comparator to sort devices by timestamp in ascending order
	public static Comparator<Device> timestampComparator = new Comparator<Device>() {
		public int compare(Device device1, Device device2) {
			Long device1Stamp = device1.timestamp;
			Long device2Stamp = device2.timestamp;
			return device1Stamp.compareTo(device2Stamp);
		    }
		};
	
}
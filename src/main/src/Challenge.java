/* Author: Craig Scheiderer
 * Date: 4/17/2017
 * Description: A driver program with functions to obtain:
 *   Total number of devices
 *   Total unique devices
 *   Number of devices that fall within the follow geographic area: 
 *      Longitude: -179.99 Latitude: 89.99, Longitude: 0.00, Latitude: 0.00
 *   Oldest update time, and what date/time that update was made
 *   Newest update time, and what date/time that update was made
 * 
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Timestamp;
import java.io.FileWriter;
import java.io.IOException;

public class Challenge {
	
	ArrayList<Device> masterList = new ArrayList<Device>();
	//This array list stores the duplicates.  Trying to remove elements from the csvArray
	//while iterating over it will cause a comodification error
	ArrayList<Device> duplicatesinCsv = new ArrayList<Device>();
	
	//Adds the size of the two un-deconflicted arrays together
	public int calculatetotalDevices(ArrayList<Device> jsonList, 
			ArrayList<Device> csvList ){
		int totaldevices = jsonList.size() + csvList.size();
		return totaldevices;
	}
	
	//Two nested for loops that compare each uuid in the jsonList with every uuid in
	//the csvList.  Big Omega (n^2) algorithm.
	public ArrayList<Device> removeDuplicates(ArrayList<Device> jsonList, 
			ArrayList<Device> csvList ){
		
		for (Device deviceJson : jsonList){
			for (Device deviceCsv : csvList){
				if (deviceJson.uuid.equals(deviceCsv.uuid)){
					duplicatesinCsv.add(deviceCsv);
				}
			}
		}
		//Items cannot be removed from csvList while iterating, so the duplicates
		//are removed from csvList after iteration
		csvList.removeAll(duplicatesinCsv);
		masterList.addAll(csvList);
		masterList.addAll(jsonList);
		
		return masterList;
	}
	
	//Compares latitude and longitude ranges of each unique device with a desired range
	public int calulatedevicesinRange(ArrayList<Device> uniqueDevices){
		int devicesinRange = 0;
		
		for (Device device : uniqueDevices){
			if ((device.longitude >= -179.99 && device.longitude <= 0.0) && 
					(device.latitude >= 0.0 && device.latitude <= 89.99)){
				devicesinRange++;
			}
		}
		
		return devicesinRange;
	}
	
	//Leverages comparator in the Device class to sort unique devices in ascending order
	//The first device will have the earliest timestamp
	public Device getoldestTime(ArrayList<Device> uniqueDevices){
		Collections.sort(uniqueDevices, Device.timestampComparator);
		Device oldestStamp = uniqueDevices.get(0); 
		return oldestStamp;
	}
	
	//Leverages comparator in the Devicen class to sort unique devices in ascending order
	//The last device has the most recent timestamp
	public Device getnewestTime(ArrayList<Device> uniqueDevices){
		Collections.sort(uniqueDevices, Device.timestampComparator);
		Device newestStamp = uniqueDevices.get(uniqueDevices.size()-1);
		return newestStamp;
	}
	
	//Writes the json string to results.json
	public void writeFile(String resultsinJson){
		try{
		FileWriter filewriter = new FileWriter("C:\\Users\\Craig\\Desktop\\results.json");
		filewriter.write(resultsinJson);
		filewriter.close();
		} catch (IOException exc) {
			exc.getMessage();
		} 
	}
	
	public static void main (String[] args){
		String csvPath = "C:\\Users\\Craig\\Desktop\\crunchyroll-challenge-988c3e8a249b\\src\\main\\resources\\data.csv";
		String jsonPath = "C:\\Users\\Craig\\Desktop\\crunchyroll-challenge-988c3e8a249b\\src\\main\\resources\\data.json";
		
		csvParser csvparser = new csvParser();
		jsonParser jsonparser = new jsonParser();
		
		csvparser.parseCsv(csvPath);
		jsonparser.parseJSON(jsonPath);
		
		Challenge challenge = new Challenge();
		
		//totalDevices should be called first before removing duplicates
		int totalDevices = challenge.calculatetotalDevices(jsonparser.jsonList, csvparser.csvList);
		challenge.removeDuplicates(jsonparser.jsonList, csvparser.csvList);
		
		int uniqueDevices = challenge.masterList.size();
		int devicesinRange = challenge.calulatedevicesinRange(challenge.masterList);
		long newestTimestamp = challenge.getnewestTime(challenge.masterList).timestamp;
		Timestamp newest = new Timestamp((challenge.getnewestTime(challenge.masterList).timestamp)*
				1000);
		Date newestDate = Date.from(newest.toInstant());
		long oldestTimestamp = challenge.getnewestTime(challenge.masterList).timestamp;
		Timestamp oldest = new Timestamp((challenge.getoldestTime(challenge.masterList).timestamp)*
				1000);
		Date oldestDate = Date.from(oldest.toInstant());
		
		Statistics statistics = new Statistics(totalDevices, uniqueDevices, devicesinRange,
				newestTimestamp, newestDate, oldestTimestamp, oldestDate);
		
		//GSON GsonBuilder class creates an object that is formatted to JSON
		//and saved as a String. 
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(statistics);
		challenge.writeFile(json);
		System.out.print(json);
		
	}
}

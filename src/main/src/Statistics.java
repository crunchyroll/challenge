/* Author: Craig Scheiderer
 * Date: 4/17/2017
 * Description: 
 */

import java.util.Date;

public class Statistics {

	int Total_devices;
	int Unique_devices;
	int Devices_in_Range;
	long Newest_update_stamp;
	Date Newest_update_Date_and_Time;
	long Oldest_update_stamp;
	Date Oldest_update_Date_and_Time;
	
	Statistics(int Total_devices, int Unique_devices, int Devices_in_Range, 
			long Newest_update_stamp, Date Newest_update_Date_and_Time, long Oldest_update_stamp, 
			Date Oldest_update_Date_and_Time){
		this.Total_devices = Total_devices;
		this.Unique_devices = Unique_devices;
		this.Devices_in_Range = Devices_in_Range;
		this.Newest_update_stamp = Newest_update_stamp;
		this.Newest_update_Date_and_Time = Newest_update_Date_and_Time;
		this.Oldest_update_stamp = Oldest_update_stamp;
		this.Oldest_update_Date_and_Time = Oldest_update_Date_and_Time;
	}
	
}

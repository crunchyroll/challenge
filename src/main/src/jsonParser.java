/* Author: Craig Scheiderer
 * Date: 4/17/2017
 * Description: 
 */

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class jsonParser {
	
	ArrayList<Device> jsonList = new ArrayList<Device>();
	
	public ArrayList<Device> parseJSON(String JSONfilePath){
		JsonParser jsonParser = new JsonParser();
		
		try {
			//create a top-level object from parsing the JSON file
			Object object = jsonParser.parse(new FileReader(JSONfilePath));
			//cast the object to a JsonObject which will allows the object to
			//be manipulated using the gson library
			JsonObject jsonObject = (JsonObject)object;
			//create a JsonArray object from the devices array in the JSON file
			JsonArray devices = (JsonArray)jsonObject.get("devices");
			
			//enhanced for loop pulls each device object from the array
			for (JsonElement device : devices){
				//This block of code takes the JsonElement from the devices array
				//and recasts it as a JsonObject.  This allows the get() method to 
				//be used to pull the separate key-value pairs representing eahc device's
				//data.  These are then used as parameters for a new device and added to the 
				//array list.
				JsonObject elementtoObj = new JsonObject();
				elementtoObj = (JsonObject)device;
				//The Json uuid is stored as a String with quotes.  In order to compare
				//uuids from each file for duplicates, the quotes need to be removed
				String uuidwithQuotes = elementtoObj.get("uuid").toString();
				String uuid = uuidwithQuotes.substring(1, uuidwithQuotes.length()-1);
				String lat = elementtoObj.get("lat").toString();
				String longt = elementtoObj.get("long").toString();
				String timestamp = elementtoObj.get("timestamp").toString();
				double latitude = Double.parseDouble(lat);
				double longitude = Double.parseDouble(longt);
				long timestamped = Long.parseLong(timestamp);
				Device deviceObj = new Device(uuid, latitude, longitude, timestamped);
				jsonList.add(deviceObj);		
			}	
		} catch (IOException exc) {
			exc.getMessage();
		}
		return jsonList;
	}
}
